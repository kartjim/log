---
title: JavaScript5-数据类型
tags: [js,前端]
date: 2021-01-14 19:14:29
category: [js,前端]
---
## 数据类型
### 数值
js有两种数值类型：
- 整数
- 浮点数
声明时不需要区分，都用 var 声明即可
```javascript
var int = 23;
var float = 3.5;
```

<!--more-->

#### 可操作数值的全局方法
|全局方法|作用|
|---|---|
|toString()|将数字转换为字符串|
|toFixed(n)|也是将数字转换为字符串,但是还可以指定小数位数|
|toExponential(n)|将数字用科学表示法表示，再转换成字符串|

举例：
```javascript
var int = 23;
var float = 3.5;

console.log(int.toString());
console.log(int.toFixed(3));
console.log(int.toExponential(4));
```
输出：

```shell
[Running] node "e:\test.js"
23
23.000
2.3000e+1
```

### Number对象
Number对象用来表示各种数值类型
Number对象有一些属性和方法，可以直接使用，不需要声明。

*Number.可以省略*

|方法|作用|
|---|---|
|Number.isNaN()|检测是否为 `非数值` |
|Number.isInteger()|检测是否为整数值|
|Number.parseFloat()|强制转换成浮点型数值|
|Number.parseInt()|强制转换成整数型数值|

### 无穷大
Infinity
and
-Infinity
### Number函数
>Number(n)函数会尽力返回一个对等的数值；如果不能返回数值，则返回NaN。


### 时间戳
新建一个Date对象并使用Number()函数转换：
```javascript
var now = new Date();
console.log(Number(now));
```
输出
```shell
[Running] node "e:\test.js"
1610624530635
```
输出的值通常叫做时间戳
- UNIX时间，从1970年1月1日00:00:00(UTC)开始所流逝的毫秒数。
- 
### 字符串
也是使用 var 来声明。
### 其它
#### 布尔值
true 、 false
#### undefined
#### null
#### 0
#### NaN
